package com.universitymanagment.universitymanagment.repository;

import com.universitymanagment.universitymanagment.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CourseRepository extends JpaRepository<Course, Long> {
}
