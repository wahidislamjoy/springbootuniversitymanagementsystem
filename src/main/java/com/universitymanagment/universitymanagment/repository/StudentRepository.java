package com.universitymanagment.universitymanagment.repository;

import com.universitymanagment.universitymanagment.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
public interface StudentRepository extends JpaRepository<Student, Long> {
}
