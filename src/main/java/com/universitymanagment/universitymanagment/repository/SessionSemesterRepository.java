package com.universitymanagment.universitymanagment.repository;

import com.universitymanagment.universitymanagment.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
public interface SessionSemesterRepository extends JpaRepository<Session, Long> {
}
