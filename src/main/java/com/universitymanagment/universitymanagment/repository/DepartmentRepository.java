package com.universitymanagment.universitymanagment.repository;

import com.universitymanagment.universitymanagment.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
