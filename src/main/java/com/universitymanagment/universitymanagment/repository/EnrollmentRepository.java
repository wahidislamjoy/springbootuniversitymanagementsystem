package com.universitymanagment.universitymanagment.repository;

import com.universitymanagment.universitymanagment.entity.Enrollment;
import org.springframework.data.jpa.repository.JpaRepository;
public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {
}
