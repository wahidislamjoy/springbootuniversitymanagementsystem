package com.universitymanagment.universitymanagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniversitymanagmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniversitymanagmentApplication.class, args);
        System.out.println("I am from main class.");
    }

}
