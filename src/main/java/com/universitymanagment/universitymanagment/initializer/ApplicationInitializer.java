package com.universitymanagment.universitymanagment.initializer;

import com.universitymanagment.universitymanagment.entity.*;
import com.universitymanagment.universitymanagment.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class ApplicationInitializer implements CommandLineRunner {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private BatchRepository batchRepository;

    @Autowired
    private SessionSemesterRepository sessionSemesterRepository;

    @Autowired
    private EnrollmentRepository enrollmentRepository;


    @Override
    public void run(String... args) throws Exception {
        //Student data
        Student student1 = new Student();
        student1.setId(1);
        student1.setName("Wahid");
        student1.setAcademicId("1302510200585");

        Student student2 = new Student();
        student2.setId(2);
        student2.setName("Sultan");
        student2.setAcademicId("1302510200586");
        //Department data
        Department department1 = new Department();
        department1.setId(1);
        department1.setName("CSE");
        departmentRepository.save(department1);

        Department department2 = new Department();
        department2.setId(2);
        department2.setName("EEE");
        departmentRepository.save(department2);
        //Course data
        Course course1 = new Course();
        course1.setId(1);
        course1.setName("Basic Programming");
        courseRepository.save(course1);

        Course course2 = new Course();
        course2.setId(2);
        course2.setName("English");
        courseRepository.save(course2);

        Course course3 = new Course();
        course3.setId(3);
        course3.setName("Mechanical");
        courseRepository.save(course3);

        Course course4 = new Course();
        course4.setId(4);
        course4.setName("Basic Programming lab");
        courseRepository.save(course4);

        Course course5 = new Course();
        course5.setId(5);
        course5.setName("Electronics one");
        courseRepository.save(course5);

        //Adding all course in the array
        List<Course> course = new ArrayList<>();
        course.add(course1);
        course.add(course2);
        course.add(course3);
        course.add(course4);
        course.add(course5);
        //Batch data
        Batch batch1 = new Batch();
        batch1.setId(1);
        batch1.setName("First Batch");
        batchRepository.save(batch1);

        Batch batch2 = new Batch();
        batch2.setId(2);
        batch2.setName("Second Batch");
        batchRepository.save(batch2);
        //Semester data
        Session session1 = new Session();
        session1.setId(1);
        session1.setName("First semester");
        sessionSemesterRepository.save(session1);

        Session session2 = new Session();
        session2.setId(2);
        session2.setName("Second semester");
        sessionSemesterRepository.save(session2);
        //Enrollment data
        Enrollment enrollment1 = new Enrollment();
        enrollment1.setId(1);
        enrollment1.setCourses(course);
        enrollment1.setStudent(student1);
        enrollment1.setSession(session1);
        enrollmentRepository.save(enrollment1);

        Enrollment enrollment2 = new Enrollment();
        enrollment2.setId(2);
        enrollment2.setCourses(course);
        enrollment2.setStudent(student2);
        enrollment2.setSession(session2);
        enrollmentRepository.save(enrollment2);
        //Batch data
        student1.setBatch(batch1);
        student1.setDepartment(department1);
        student1.setEnrollment(enrollment1);
        studentRepository.save(student1);

        student2.setBatch(batch2);
        student2.setDepartment(department2);
        student2.setEnrollment(enrollment2);
        studentRepository.save(student2);


    }
}
